package listener;

import communication.Reader;
import datahandler.DataSaver;
import event.Event;
import objects.CommentObject;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static datahandler.DataSaver.setBufferSize;


public class CommandPromptListener implements Runnable {
    private DataSaver dataSaver;
    public CommandPromptListener(DataSaver dataSaver) {
    this.dataSaver = dataSaver;
    }
    private Reader reader;
    private DateFormat dateFormat = new SimpleDateFormat("yyMMdd_HHmmss");
    private String configFileName = "none";

    @Override
    public void run() {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line = "";
        askMetaInfo();

        while (!line.equalsIgnoreCase("quit")) {
            try {
                line = in.readLine().toUpperCase();
            } catch (IOException e) {
                e.printStackTrace();
            }

            switch (line) {
                case "K":
                    System.out.println("Add a comment");
                    String dateString = dateFormat.format(new Date());
                    reader.setCommentID("K:" + dateString);
                    String comment = System.console().readLine();
                    CommentObject commentObject = new CommentObject();
                    commentObject.setComment(comment);
                    commentObject.setCommentID("K:" + dateString);
                    dataSaver.addComment(commentObject);
                    break;

                case "/QUIT":
                    System.out.println("Quitting application");
                    reader.setSaveDataFlag(true);
     //               dataSaver.setWriteComments(true);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dataSaver.setListenToDataFlag(false);
                    System.exit(0);
                case "/HELP":
                    helpMenu();
                    break;
                default:

            }
        }
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void helpMenu() {
        System.out.println("k - For adding comments");
        System.out.println("/quit - Ends work of the program");
        System.out.println("Typing /help will display this menu");
        //Once metainformation is set, data is starting to be saved.
        dataSaver.setListenToDataFlag(true);
        reader.setCurrentEvent(Event.MICROWAVE);
    }

    public void askMetaInfo() {
        boolean correctInfo = false;
        while (!correctInfo) {
            System.out.println("Insert meta information:");
    //        System.out.println("Metainfo will be read in from the first line of config.txt");
    //        System.out.println("Insert frequency:");
    //        System.out.println("long,lat,height,repo_nr,Hz;humidity(%), temp(Celsius), pressure(mmHg)," +
    //                " wind(m/s), sunlight(True/false), rain(true/false),snow(True/false);object;");

            /*
            System.out.println("Insert longitude");
            String longitude = System.console().readLine();
  //          dataSaver.setLongitude(Double.parseDouble(longitude));  //TODO lahtikommenteerida
            System.out.println("Insert latitude");
            String latitude = System.console().readLine();
 //           dataSaver.setLatitude(Double.parseDouble(latitude)); //TODO see ka
            System.out.println("Insert height(m)");
            String height = System.console().readLine();
            System.out.println("Insert repo number");
            String repoNumber = System.console().readLine();
            System.out.println("Insert frequency(Hz)");
            String frequency = System.console().readLine();
            System.out.println("Insert humidity(%)");
            String humidity = System.console().readLine();
            System.out.println("Insert temperature(Celsius)");
            String temperature = System.console().readLine();
            System.out.println("Insert pressure(mmHg)");
            String pressure = System.console().readLine();
            System.out.println("Insert wind strength(m/s)");
            String wind = System.console().readLine();
            System.out.println("Sunlight (true/false)");
            String sunlight = System.console().readLine();
            System.out.println("Rain (true/false)");
            String rain = System.console().readLine();
            System.out.println("Snow (true/false)");
            String snow = System.console().readLine();


            String metaInfoInput = longitude + "," + latitude + "," + height + "," + repoNumber + "," + frequency + ";"
                    + humidity + "," + temperature + "," + pressure + "," + wind + "," + sunlight + "," + rain + "," +
                    snow + ";"; */
 /*           BufferedReader r = null;
            try {
                r = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/microwave/" + "config.txt"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            String metaInfoInput = null;
            try {
                metaInfoInput = r.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (metaInfoInput == null) metaInfoInput = "METAINFO_NOT_ADDED";  */
            Map<String, String> map  = askForConfigFile();
            String metaInfoInput = map.toString();

            System.out.println("Your meta information input was:");
            System.out.println(metaInfoInput);
            System.out.println("If that was correct enter 'Y', otherwise enter a random character");
            String confirmation = System.console().readLine().toUpperCase();
            double frequency = Integer.parseInt(map.get("Frequency"));

            if (confirmation.equals("Y")) {
                askForRecordingLength(frequency);
                System.out.println("Press enter to start recording");
                System.console().readLine();
                reader.setSaveDataFlag(true);
                correctInfo = true;
        //        dataSaver.setMetaInfo(metaInfoInput);
                dataSaver.setMetaInfo(configFileName);
                helpMenu();

            }
        }
    }

    public Map<String, String> askForConfigFile() {
        System.out.println("Enter the name of config file in testsettings folder");
        String configFile = System.console().readLine();
        configFileName = configFile;
        Map<String, String> map = parseConfigFile(configFile);
        return map;
        }



    public Map<String, String> parseConfigFile(String configFile) {
        Map<String, String> map = new HashMap<>();
        BufferedReader  bfr = null;
        try {
            bfr = new BufferedReader(new FileReader(new File(System.getProperty("user.dir")
                    + "/testsettings/" + configFile)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String line = null;
        while (true) {
            try {
                if ((line = bfr.readLine()) == null) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (!line.isEmpty()) {
                String[] pair = line.trim().split("=");
                map.put(pair[0].trim(), pair[1].trim());
            }
        }
        return map;
    }


    private static void askForRecordingLength(Double frequency) {
        System.out.println("How long should data recording length be? (seconds)");
        String recLength = System.console().readLine();
        double recordingLength = Double.parseDouble(recLength);
        setBufferSize((int) ( recordingLength * frequency));
    }

    public void setReader(Reader reader) {
        this.reader = reader;
    }
}
