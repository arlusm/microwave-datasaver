package communication;

import com.sun.javafx.image.ByteToBytePixelConverter;
import objects.DataObject;
import event.Event;
import sun.misc.IOUtils;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.BlockingQueue;


public class Reader implements Runnable {


    public static final int FRAME_LENGTH = 100;
    //kui väärtused on üle selle siis skipitakse 1 byte
    public static final int LARGEST_ALLOWED_VALUE_IN_AUDIO = 4096;
    private InputStream in;
    private BlockingQueue<DataObject> queue;
    private boolean saveDataFlag = false;
    private Event currentEvent = Event.MICROWAVE;
    private LocalDateTime lastReceivedDate = LocalDateTime.now();
    private boolean timeWarningFlag = false;

    private String commentID = "";


    private DataObject dataList = new DataObject();

    Reader(InputStream in, BlockingQueue<DataObject> queue) {
        this.queue = queue;
        this.in = in;
    }

    public void run() {
        DataInputStream dataInputStream = new DataInputStream(in);
        gatherData(dataInputStream);

       /* try {
            gatherBytes(in);
        } catch (IOException e) {
            e.printStackTrace();
        } */

    }


    public void gatherBytes(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[20480];
        ByteBuffer bb = ByteBuffer.allocate(20480);
        bb.order(ByteOrder.BIG_ENDIAN);
        int readBytes = -1;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int counter=-1000;
        while (true) {
            while (inputStream.available() > 0) {
                counter++;
                bb.put((byte) inputStream.read());
//            baos.write(buffer,0,readBytes);
                // System.out.println("write: " + readBytes);
                if (counter >= 0) {
                    short shortVal = bb.getShort(counter);
                    //int intVal = shortVal >= 0 ? shortVal : 0x10000 + shortVal;

                    System.out.println(shortVal);
                }
            }
            byte[] responseArray = baos.toByteArray();
        }
    }

    public void gatherData(DataInputStream dataInputStream) {
           int counter = 0;
        while (true) {
            try {
                if (ChronoUnit.SECONDS.between(lastReceivedDate, LocalDateTime.now()) > 3 && !timeWarningFlag) {
                        System.out.println("Data hasn't been forwarded to the port for over 3 seconds");
                        timeWarningFlag = true;
                    }
                    while (in.available() > 0) {
                    //    if (!(in.available() % 2 == 0)) dataInputStream.skipBytes(1);
                        //System.out.println("Available bytes?: " + in.available());
                        timeWarningFlag = false;
                        int twoBytesOfData = dataInputStream.readUnsignedShort();
                        counter++;
                        dataList.getSoundData().add(twoBytesOfData);
                   //     dataOutputStream.writeBytes(twoBytesOfData + "\n");

                        if (ChronoUnit.MILLIS.between(lastReceivedDate, LocalDateTime.now()) > 600) {
                            //System.out.println("counter:" + counter);
                            counter = 0;
                            //System.out.println(ChronoUnit.MILLIS.between(lastReceivedDate, LocalDateTime.now()));
                        }
                        lastReceivedDate = LocalDateTime.now();
                        if (!getCommentID().equals("")) {
                            dataList.setCommentID(commentID);
                            commentID = "";
                        }
                        if (twoBytesOfData > LARGEST_ALLOWED_VALUE_IN_AUDIO) {
                            //          counter++;
                            dataInputStream.skipBytes(1);
                            dataList.getSoundData().remove(dataList.getSoundData().size() - 1);
                        }
                        if (isSaveDataFlag() && !getCurrentEvent().equals(Event.UNINITIALIZED)) {
                            dataList.setSaveImmediately(true);
                      //      queue.put(dataList);
                            dataList = new DataObject();
                            dataList.setEventFlag(currentEvent);
                            setSaveDataFlag(false);
                        }
                        if (dataList.getSoundData().size() >= FRAME_LENGTH) {
                            queue.put(dataList);
                            dataList = new DataObject();
                            dataList.setEventFlag(currentEvent);
                        }
                    }

            } catch (IOException | InterruptedException e) {
                System.out.println(e);
                try {
                    dataInputStream.close();
             //       dataOutputStream.flush();
                    in.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            //    gatherData(dataInputStream);

            }
        }
    }

    public synchronized String getCommentID() {
        return commentID;
    }

    public synchronized void setCommentID(String commentID) {
        this.commentID = commentID;
    }

    public synchronized boolean isSaveDataFlag() {
        return saveDataFlag;
    }

    public synchronized void setSaveDataFlag(boolean saveDataFlag) {
        this.saveDataFlag = saveDataFlag;
    }

    public Event getCurrentEvent() {
        return currentEvent;
    }

    public void setCurrentEvent(Event currentEvent) {
        this.currentEvent = currentEvent;
    }
}