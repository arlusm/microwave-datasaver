package communication;
import com.fazecast.jSerialComm.SerialPort;
import objects.DataObject;
import listener.CommandPromptListener;

import java.io.InputStream;
import java.util.concurrent.BlockingQueue;

public class Communicator {

    private static final int TIMEOUT = 500;
    private BlockingQueue<DataObject> queue;
    private CommandPromptListener commandPromptListener;

    public Communicator(BlockingQueue<DataObject> queue) {
        this.queue = queue;
    }

    public void connect(String portName) throws Exception {

        SerialPort comPort = SerialPort.getCommPort(portName);
        comPort.openPort();
        comPort.setBaudRate(921600);
        comPort.setNumDataBits(8);
        comPort.setNumStopBits(1);
        comPort.setParity(SerialPort.NO_PARITY);
        comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 0, 0);
        InputStream in = comPort.getInputStream();
        Reader reader = new Reader(in, queue);
        commandPromptListener.setReader(reader);
        new Thread(reader).start();
    }

       /*     CommPortIdentifier portIdentifier = CommPortIdentifier
                    .getPortIdentifier(portName);
            if (portIdentifier.isCurrentlyOwned()) {
                throw new Exception("Port currently in use");
            }
            CommPort commPort = portIdentifier.open("DataSaver", TIMEOUT);

            SerialPort serialPort = (SerialPort) commPort;
            serialPort.setSerialPortParams( 921600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE );
            InputStream in = serialPort.getInputStream();
          //  InputStream in2 = commPort.getInputStream();
            Reader reader = new Reader(in, queue);
        commandPromptListener.setReader(reader);
        new Thread(reader).start();
            } */

    public void setCommandPromptListener(CommandPromptListener commandPromptListener) {
        this.commandPromptListener = commandPromptListener;
    }
}
