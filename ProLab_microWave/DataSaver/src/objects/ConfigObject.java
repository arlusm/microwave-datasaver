package objects;

public class ConfigObject {
    double latitude;
    double longitude;
    double height;
    String repo_nr;
    double frequency;
    double humidity;
    double temperature;
    double pressure;
    double windStrength;
    boolean sunlight;
    boolean rain;
    boolean snow;

    public ConfigObject(double latitude, double longitude, double height, String repo_nr, double frequency, double humidity, double temperature, double pressure, double windStrength, boolean sunlight, boolean rain, boolean snow) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.height = height;
        this.repo_nr = repo_nr;
        this.frequency = frequency;
        this.humidity = humidity;
        this.temperature = temperature;
        this.pressure = pressure;
        this.windStrength = windStrength;
        this.sunlight = sunlight;
        this.rain = rain;
        this.snow = snow;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getRepo_nr() {
        return repo_nr;
    }

    public void setRepo_nr(String repo_nr) {
        this.repo_nr = repo_nr;
    }

    public double getFrequency() {
        return frequency;
    }

    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public double getWindStrength() {
        return windStrength;
    }

    public void setWindStrength(double windStrength) {
        this.windStrength = windStrength;
    }

    public boolean isSunlight() {
        return sunlight;
    }

    public void setSunlight(boolean sunlight) {
        this.sunlight = sunlight;
    }

    public boolean isRain() {
        return rain;
    }

    public void setRain(boolean rain) {
        this.rain = rain;
    }

    public boolean isSnow() {
        return snow;
    }

    public void setSnow(boolean snow) {
        this.snow = snow;
    }
}
